FROM python:3.10-alpine

WORKDIR /flask_app

COPY ./flask_app .

RUN apk add --update --no-cache --virtual .tmp-build-deps \
    gcc libc-dev linux-headers && \
    pip install --no-cache-dir -r requirements.txt

ENTRYPOINT ["flask"]
CMD ["run", "--host=0.0.0.0", "--port=5000"]
