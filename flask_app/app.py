from pyfiglet import Figlet
from flask import Flask

import os

app = Flask(__name__)

font = Figlet(font="isometric1")

@app.route('/')
def hello_world():
    message = os.getenv("MESSAGE", "no message specified")
    html_text = font.renderText(message)\
        .replace(" ","&nbsp;")\
        .replace(">","&gt;")\
        .replace("<","&lt;")\
        .replace("\n","<br>")
    return "<html><body style='font-family: mono;'>" + html_text + "</body></html>", 200

if __name__ == '__main__':
  app.run(debug=True, host='0.0.0.0')
